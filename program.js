var fs=require('fs');
var https = require("https");

var key = "dk4xITS_xeOabGOYxXMonRYAV0YJvRmb";

var boundaries = /-----BEGIN CERTIFICATE-----[\s\S]+?-----END CERTIFICATE-----\n/g
var certs = fs.readFileSync("./cacert.pem").toString()
https.globalAgent.options.ca = certs.match(boundaries);

var options = require("url").parse("https://api.tinypng.com/shrink");
options.auth = "api:" + key;
options.method = "POST";

var inputpath= './images';
var outputpath= './output';
var data={};
var count=0;
var queue=[];

function tinyDir(dir) {
    console.log("====================dir:" + dir);
    fs.readdir(inputpath + "/" + dir,function(err,files){
               if (err) throw err;
               files.forEach(function(file){
                             if(file.indexOf('.DS') > -1) {
                                //ignore
                             } else if(file.indexOf('.png') > -1) {
                                //enqueue to get file
                                queue.push(dir+"/"+file);
                             } else {
                                //search in dir
                                tinyDir(dir + "/" +file);
                             }
                });
               processQueue();
   });
}

function tinyPng (path) {
    console.log("file: "+path);
    count++;
    
    var input = fs.createReadStream(inputpath    + "/" + path);
    var output = fs.createWriteStream(outputpath + "/" + path);
    var request = https.request(options, function(response) {
                                if (response.statusCode === 201) {
                                   /* Compression was successful, retrieve output from Location header. */
                                   https.get(response.headers.location, function(response) {
                                             response.on('end', function() {
                                                    queue.splice(0, 1);
                                                         processQueue();
                                              });
                                          response.pipe(output);
                                    });
                                } else {
                                   var body = '';
                                   response.on('data', function(d) {
                                            body += d;
                                            });
                                   response.on('end', function() {
                                            var parsed = JSON.parse(body);
                                            console.log("data: "+parsed.message);
                                   });
                                }
                            });
    input.pipe(request);
}

function processQueue() {
    if (queue.length>0) {
        tinyPng(queue[0])
    } else {
        console.log("Finish");
        process.exit();
    }
}

tinyDir(process.argv[2]);
//node program.js <folder name under [inputpath]>
//example : node program.js item